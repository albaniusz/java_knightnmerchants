package local.knightnmerchants.game.entities.person;

import local.knightnmerchants.game.entities.EntityAbstract;

public abstract class PersonAbstract extends EntityAbstract implements Person {
	protected Short cost = 1;
//	protected Resource//TODO curency, of Resource

	public Short getCost() {
		return cost;
	}

	public void setCost(Short cost) {
		this.cost = cost;
	}
}
