package local.knightnmerchants.game.entities;

public abstract class EntityAbstract implements Entity {
	protected String name;
	protected String ico;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIco() {
		return ico;
	}

	public void setIco(String ico) {
		this.ico = ico;
	}
}
